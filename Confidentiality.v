Require Import List.
Include ListNotations.

Require Import Lia.

Parameter Component: Type.
Parameter Message: Type.
Parameter Payload : Type.


Parameter Pld: Message -> Payload.
Parameter Src: Message -> Component.
Parameter Rcv: Message -> Component.


(*
  elementary actions
  - inject a message in the system
  - intercept a message from the system
  - get message receiver, sender or payload
  - set message receiver, sender or payload
*)
Inductive Action: Type :=
    inject (m:Message)
  | intercept (m:Message)
  | get_rcv (m:Message) (c:Component)
  | get_src (m:Message) (c:Component)
  | get_pld (m:Message) (p:Payload)
  | set_rcv (m:Message) (c:Component)
  | set_src (m:Message) (c:Component)
  | set_pld (m:Message) (p:Payload).

(*
compound actions describe sets of sequences of actions
 - a given elementary action
 - any sequence of actions
 - the union of two sets of actions
 - the sequence of two actions taken from two sets of actions
 - the repetition of actions taken from a set of actions
 - one elementary action
*)

Inductive CompoundAction : Type :=
    BasicAction (c:Component) (a : Action)
  | Any
  | UnionAction (a1 a2 : CompoundAction)
  | SequenceAction (a1 a2 : CompoundAction)
  | RepeatAction (a: CompoundAction)
  | SkipAction.


(*
  A compound action does not contain sequences
*)
Fixpoint noSequence ca :=
  match ca with
    BasicAction _ _ => True
  | Any => True
  | UnionAction ca1 ca2 => noSequence ca1 /\ noSequence ca2
  | SequenceAction _ _ => False
  | RepeatAction ca' => noSequence ca'
  | SkipAction => True
  end.

(*
  A compound action contains the empty action
*)

Fixpoint hasEmpty ca :=
  match ca with
    BasicAction _ _ => False
  | Any => True
  | UnionAction ca1 ca2 => hasEmpty ca1 \/ hasEmpty ca2
  | SequenceAction ca1 ca2 => hasEmpty ca1 /\ hasEmpty ca2
  | RepeatAction ca' => True
  | SkipAction => False
  end.

(* A behavior is an infinite sequence of actions performed by components *)

Definition Behavior := nat -> (Component * Action).

Notation "c 'does' a" := (BasicAction c a) (at level 23,  no associativity).

(* atomic properties *)
Inductive Atom :=
| has_src (m: Message) (c: Component) (* c is the source of m *)
| has_rcv (m: Message) (c: Component) (* c is the destination of m *)
| has_pld (m: Message) (p: Payload) (* m contains payload p *)
| enable (c: Component) (a: Action). (* action a is enabled for component c *)

(* The property language is an action modal logic over atomic properties *)
Inductive Formula :=
| AtomicFormula (a:Atom)
| IsFalse
| Implies (f1: Formula) (f2: Formula)
| Until (f1: Formula) (A: CompoundAction) (f2: Formula).

Coercion AtomicFormula: Atom >-> Formula.

(* derived properties *)
Definition Not p := Implies p IsFalse.
Definition IsTrue := Not IsFalse.
Definition Or p q := Implies (Not p) q.
Definition And p q := Not (Or (Not p) (Not q)).
Definition Eventually A f := Until IsTrue A f.
Definition Globally A f := Not (Eventually A (Not f)).
Definition Leadsto f1 A f2 := Globally Any (Implies f1 (Eventually A f2)).
Definition H c a := Eventually (c does a) IsTrue.
Definition E c a := enable c a.
Definition Precede p A q := Not (Until (Not p) A q).

Notation "p ==> q" := (Implies p q) (at level 14, right associativity).
Notation "p --> q" := (Leadsto p Any q) (at level 49, right associativity).
Notation "p -[ A ]-> q" := (Leadsto p A q) (at level 49, right associativity).
Notation "p << q" := (Precede p Any q) (at level 15, no associativity).
Notation "p <[ A ]< q" := (Precede p A q) (at level 15, no associativity).
Notation "p || q" := (Or p q).
Notation "p && q" := (And p q).
Notation "! p" := (Not p) (at level 9, right associativity).

(*
  The actual behavior of components is not modelled. Thus the semantics 
  of basic properties is seen as a parameter of the theory.
*)
Parameter asat: (Component * Action) -> Atom -> Prop.
(*
 the semantics should satisfy the following property: if an action is performed by a component, 
 it was enabled for this component.
*)
Parameter asat_E: forall c a, asat (c,a) (E c a).

(*
  compat B t ca: does behavior B until date t conforms with action set ca.

- BasicAction c a : B [..t[ contains exactly this action
- Any: every behavior is allowed
- Union ca1 ca2: B[..t[ conforms to ca1 or ca2
- SequenceAction ca1 ca2: B[..u[ conforms to ca1 and B[u..t[ conforms to ca2
- RepeatAction ca': [..t[ can be split into n segments such as the restriction of B to each segment conforms to ca'.
*)
Fixpoint compat B t ca :=
  match ca with
    BasicAction c a => B 0 = (c,a) /\ t = 1
  | Any => True
  | UnionAction ca1 ca2 => compat B t ca1 \/ compat B t ca2
  | SequenceAction ca1 ca2 => exists u, u <= t /\ compat B u ca1 /\ compat (fun i => B (i+u)) (t-u) ca2
  | RepeatAction ca' => exists f n,
    (forall i, i < n -> f i < f (i+1)) /\
    f n = t /\
    (forall i, i < n -> compat (fun u => B (u+f i)) (f (S i) - f (i)) ca')
  | SkipAction => t=1
  end.

(*
  an empty behavior conforms with an action set containing the empty action.
*)
Lemma compat_empty: forall ca B, hasEmpty ca -> compat B 0 ca.
Proof.
  induction ca; simpl in *; intros; auto; try tauto; try firstorder.
  exists (fun i => i); exists 0; repeat split; intros; auto; lia.
Qed.

(*
sat B f: the behavior B satisfies the formula f
*)
Fixpoint sat B f : Prop :=
  match f with
   AtomicFormula a => asat (B 0) a
 | IsFalse => False
 | Implies f1 f2 => (sat B f1) -> (sat B f2)
 | Until f1 A f2 => exists t,
    sat (fun i => B (t+i)) f2 /\
    (forall t', t' < t -> sat (fun i => B (t'+i)) f1) /\
    compat B t A 
  end.

Notation "B |= f" := (sat B f) (at level 50, no associativity).

(*
Axiomatisation of basic actions: only actual information can be retreived.
- if has_src m s is eventually true, s is the actual source of m.
- if has_rcv m r is eventually true, r is the actual receiver of m.
- if has_pld m r is eventually true, r is the actual payload of m.
*)
Axiom HasSrc : forall B A s m, (B |= Eventually A (has_src m s)) -> s = Src m.
Axiom HasRcv : forall B A r m, (B |= Eventually A (has_rcv m r)) -> r = Rcv m.
Axiom HasPld : forall B A p m, (B |= Eventually A (has_pld m p)) -> p = Pld m.

(*
  proof of basic semantic properties of proppositional and temporal operators
*)

Lemma satNot_elim: forall B p, (B |= !p) -> not (B |= p).
Proof.
  intros; assumption.
Qed.
Lemma satImpl_elim: forall B p q, (B |= p ==> q) -> ((B |= p) -> (B |= q)).
Proof.
  intros.
  apply H0.
  apply H1.
Qed.
Lemma satImpl_intro: forall B p q, ((B |= p) -> (B |= q)) -> (B |= p ==> q).
Proof.
  intros.
  simpl; auto.
Qed.
Require Import Classical.
Lemma satAnd_elim: forall B p q, (B |= p && q) -> ((B |= p) /\ (B |= q)).
Proof.
  intros.
  simpl in H0.
  apply NNPP; intro.
  auto.
Qed.
Lemma satEventuallyAnd_elim: forall B p q A, (B |= Eventually A (p && q)) -> ((B |= Eventually A p) /\ (B |= Eventually A q)).
Proof.
  intros.
  destruct H0 as [t H0].
  destruct H0.
  apply NNPP; intro.
  simpl in H2.
  induction H2.
  split; exists t; split; intros; auto; clear H1.
  apply satAnd_elim in H0; apply H0.
  apply satAnd_elim in H0; apply H0.
Qed.
Lemma eventually_imply: forall p q A, (forall B, B |= p ==> q) -> forall B, B |= Eventually A p ==> Eventually A q.
Proof.
  intros.
  apply satImpl_intro; intro.
  simpl in H1.
  destruct H1 as [t H1].
  destruct H1.
  specialize (H0 (fun i : nat => B (t + i))).
  exists t; split; auto.
Qed.
Lemma gImp_eventually: forall B p q A, (B |= Globally A (p ==> q)) -> (B |= Eventually A p) -> (B |= Eventually A q).
Proof.
  intros.
  destruct H1 as [t H1]; destruct H1.
  exists t; split; auto.
  simpl in H0.
  apply NNPP; intro.
  apply H0; clear H0.
  exists t; split; intros; auto.
Qed.

(*
 si A does not contain sequences, 
 the prefix of a compatible behavior is also compatible.
*)

Lemma noSequence_compat: forall A B t1 t2, noSequence A -> 0 < t1 <= t2 -> compat B t2 A -> compat B t1 A.
Proof.
  induction A; simpl; intros; auto.
  - split; try tauto; lia.
  - firstorder.
  - firstorder.
  - destruct H2 as [f H2]; destruct H2 as [n H2]; destruct H2 as [H2 H3]; destruct H3 as [H3 H4].
    revert n t2 t1 f H1 H2 H3 H4; induction n; intros; auto.
    exists (fun i => i+t1); exists 0; repeat split; intros; auto; lia.

    destruct (PeanoNat.Nat.eq_dec t1 t2).
    subst t1.
    exists f; exists (S n); repeat split; intros; now auto.
    
    assert (t1 <= f n \/ t1 > f n) by lia.
    destruct H5.
    
    destruct (IHn (f n) t1 f) as [g [m h] ]; intros; auto; try tauto; try lia.
    exists g; exists m; repeat split; intros; auto; try tauto; try (apply h; tauto).

    generalize (H4 n (PeanoNat.Nat.lt_succ_diag_r n)); intro Hn.
    rewrite H3 in Hn.
    apply (IHA (fun u : nat => B (u + f n)) (t1-f n) (t2-f n) ) in Hn; auto; try lia.

    set (g i := if PeanoNat.Nat.eq_dec i (S n) then t1 else f i).
    exists g; exists (S n); repeat split; intros; auto.
    * unfold g.
      destruct (PeanoNat.Nat.eq_dec i (S n)).
      subst i; lia.
      destruct (PeanoNat.Nat.eq_dec (i + 1) (S n) ); auto.
      specialize (H2 n (PeanoNat.Nat.lt_succ_diag_r n)).
      assert (i=n) by lia.
      subst i; lia.
    * unfold g.
      destruct (PeanoNat.Nat.eq_dec (S n) (S n)); tauto.
    * assert (forall i, i <= n -> g i = f i).
      intros; unfold g.
      destruct (PeanoNat.Nat.eq_dec i0 (S n)); auto; subst i0; lia.
      rewrite (H7 i); try lia.
      assert (i=n \/ i < n) by lia.
      destruct H8.
      subst i.
      unfold g.
      destruct (PeanoNat.Nat.eq_dec (S n) (S n)); tauto.
      rewrite (H7 (S i)); try lia.
      apply H4; auto.
  - lia.
Qed.

(*
  if p precedes q and q eventually happens, then p eventually happens.
  This is only true if atomic sequences are not hidden.
*)

Lemma prec_eventually: forall B p q A
  (Hns: noSequence A) (Heps: hasEmpty A), (B |= p <[ A ]< q) -> (B |= Eventually A  q) -> (B |= Eventually A p).
Proof.
  intros.
  simpl in *.
  destruct H1 as [t [H1 [H2 H3] ] ].
  apply NNPP; intro.
  apply H0; clear H0.
  exists t; repeat split; intros; auto.
  apply H4; clear H4.
  exists t'; repeat split; auto.
  destruct t'.
  now apply compat_empty.
  revert H3; apply noSequence_compat; auto.
  lia.
Qed.

Lemma HImpE : forall B c1 p, B |= (H c1 p) ==> (E c1 p).
Proof.
  intros.
  unfold H; intro.
  simpl in H0.
  apply NNPP; intro.
  decompose record H0; clear H0.
  apply H1.
  simpl.
  rewrite H4.
  apply asat_E.
Qed.

(*
Computation model axioms : 
- if a component c1 inject a message m, then before m was injected by a component c_2
- if a component c1 is able to get the payload d of message m, then before a component c1 intercepted a message m that contained a payload d
*)
Axiom axiomInjectPrecIntercept : forall B c1 c2 m, B |= H c2 (inject m) << E c1 (intercept m).
Axiom axiomInterceptHasPldImpEGetPld : forall B c m d, B |= (H c (intercept m) && (has_pld m d)) << E c (get_pld m d).



Definition PayloadConfidenciality B c1 c2 :=
  forall c3 m d, not (In c3 [c1; c2]) -> (B |= Eventually Any (E c3 (get_pld m d))) ->
    not (B |= (H c1 (inject m) && has_rcv m c2) << (E c3 (get_pld m d))).

Record AllowedGetPld : Type :=  {
  msg: Message ;
  comp : Component;
}.

Definition restrictiveGetPld (p:AllowedGetPld) m B :=
  (forall c, (B |= Eventually Any (E c (inject m)) -> (msg p = m /\ Rcv m = comp p)))
    /\
  (forall c d, (B |= Eventually Any (E c (get_pld m d))) -> (msg p = m /\ c = comp p)). (* When get_pld is done respect the policy *)


Definition connectorRestritiveGetPld p B :=
  forall c m, (B |= Eventually Any (E c (intercept m))) -> restrictiveGetPld p m B.


Theorem confidentialityHold:
  forall B c1 c2 p, connectorRestritiveGetPld p B -> PayloadConfidenciality B c1 c2.
Proof.
  (* step 1 *)
  intros B c1 c2 p connector_restritive_get_pld.
  unfold PayloadConfidenciality, connectorRestritiveGetPld, restrictiveGetPld  in *.
  intros c3 m d c3_not_in_c1_c2 c3_eventually_enable_to_get_d c1_injected_m_to_c2.
  specialize (connector_restritive_get_pld c3 m).
  
  (* step 2 *)
  generalize (axiomInterceptHasPldImpEGetPld B c3 m d). intro axiomInterceptHasPldImpGetPld_c3_m_d.
  generalize (prec_eventually _ _ _ Any I I axiomInterceptHasPldImpGetPld_c3_m_d c3_eventually_enable_to_get_d). clear axiomInterceptHasPldImpGetPld_c3_m_d;
  intro c3_eventually_intercept_m_and_hasPld_m_d.
  apply satEventuallyAnd_elim in c3_eventually_intercept_m_and_hasPld_m_d.
  destruct c3_eventually_intercept_m_and_hasPld_m_d as [c3_eventually_intercept_m _].
  
  (* step 3 *)
  generalize(fun B => HImpE B c3 (intercept m)); intro axiomHImpE_c3_intercept_m.
  generalize (eventually_imply _ _ Any axiomHImpE_c3_intercept_m B); clear axiomHImpE_c3_intercept_m;
  intro ev_c3_intercept_m_impl_enable_c3_intercept_m.
  generalize (satImpl_elim B _ _ ev_c3_intercept_m_impl_enable_c3_intercept_m c3_eventually_intercept_m);
  clear ev_c3_intercept_m_impl_enable_c3_intercept_m ev_c3_intercept_m_impl_enable_c3_intercept_m; 
  intro c3_eventually_e_intercept_m.  
  
  (* step 4 *)
  apply connector_restritive_get_pld in c3_eventually_e_intercept_m as restritive_get_pld; clear connector_restritive_get_pld c3_eventually_e_intercept_m.
  
  
  (* step 5 *)
  destruct restritive_get_pld as [eventually_e_inj_m_impl_valid_policy eventually_e_get_m_d_impl_valid_policy]. 
  specialize (eventually_e_inj_m_impl_valid_policy c1).
  specialize (eventually_e_get_m_d_impl_valid_policy c3 d).
  destruct (eventually_e_get_m_d_impl_valid_policy c3_eventually_enable_to_get_d) ; clear eventually_e_get_m_d_impl_valid_policy.
  subst.
  
  (* step 6 *)
  generalize(axiomInterceptHasPldImpEGetPld B (comp p) (msg p) d); intro axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d.
  generalize (prec_eventually _ _ _ Any I I axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d c3_eventually_enable_to_get_d). 
  clear axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d.
  intro eventually_h_comp_p_intercept_msg_p.
  apply satEventuallyAnd_elim in eventually_h_comp_p_intercept_msg_p.
  destruct eventually_h_comp_p_intercept_msg_p as [eventually_h_comp_p_intercept_msg_p _].
  
  
  (* step 7 *)
  generalize (axiomInjectPrecIntercept B (comp p) c1 (msg p)); intro axiomInjectPrecIntercept_comp_p_c1_msg_p_d.
  generalize(fun B => HImpE B (comp p) (intercept (msg p))); intro axiomHImpE_comp_p_intercept_msg_p.
  generalize (eventually_imply _ _ Any axiomHImpE_comp_p_intercept_msg_p B); 
  clear axiomHImpE_comp_p_intercept_msg_p; 
  intro ev_H_compP_intercept_msgP.
  generalize (satImpl_elim B _ _ ev_H_compP_intercept_msgP eventually_h_comp_p_intercept_msg_p); 
  clear ev_H_compP_intercept_msgP;
  intro H_c1_inject_msgP_impl_E_c1_inject_msgP.
  generalize (prec_eventually _ _ _ Any I I axiomInjectPrecIntercept_comp_p_c1_msg_p_d H_c1_inject_msgP_impl_E_c1_inject_msgP).
  clear axiomInjectPrecIntercept_comp_p_c1_msg_p_d H_c1_inject_msgP_impl_E_c1_inject_msgP; 
  intro ev_c1_inject_msgP.
  
  
  (* step 8 *)
  generalize(fun B => HImpE B c1 (inject (msg p))); intro axiomHImpE_c1_inject_msgP.
  generalize (eventually_imply _ _ Any axiomHImpE_c1_inject_msgP B); 
  clear axiomHImpE_c1_inject_msgP;
  intro H_c1_inject_msgP_impl_E_c1_inject_msgP.
  generalize (satImpl_elim B _ _ H_c1_inject_msgP_impl_E_c1_inject_msgP ev_c1_inject_msgP);
  clear H_c1_inject_msgP_impl_E_c1_inject_msgP;
  intro E_c1_inject_msgP.
  
  (* step 9 *)
  generalize (eventually_e_inj_m_impl_valid_policy E_c1_inject_msgP).
  clear E_c1_inject_msgP;
  intro policy.
  destruct policy as [_ policyRight].
  rewrite <- policyRight in *; clear policyRight.
  
  (* step 10 *)
  generalize (prec_eventually _ _ _ Any I I c1_injected_m_to_c2 c3_eventually_enable_to_get_d). 
  clear c1_injected_m_to_c2 c3_eventually_enable_to_get_d; 
  intro ev_h_c1_inject_msgP_and_hasRcv_msgP_c2.
  apply satEventuallyAnd_elim in ev_h_c1_inject_msgP_and_hasRcv_msgP_c2.
  destruct ev_h_c1_inject_msgP_and_hasRcv_msgP_c2 as [_ ev_hasRcv_msg2_c2].
  apply HasRcv in ev_hasRcv_msg2_c2.
  subst.
  apply c3_not_in_c1_c2.
  simpl.
  auto.
Qed.

